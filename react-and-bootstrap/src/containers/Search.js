import React, { Component } from 'react';
import './Search.css';

class Search extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <img id="logo" src="/assets/logo-nav@3x.png" />
                    <a href="/Search2.js"><i class="fas fa-search" id="search" /></a>
                    <p>Clique na busca para iniciar.</p>
                </nav>
            </div>
        );
    }
}

export default Search;