import React, { Component } from 'react';
import './Login.css';
import { withRouter } from 'react-router-dom';

class Login extends Component {

    constructor(props) {
        super(props);
    }

    submitForm(e) {
        e.preventDefault()
        this.props.history.push('/components/Search.js');
    }

    render() {
        
        return (
            <div className="container">
                <div className="row">
                    <img src="assets/logo-home@3x.png" id="logo-login" alt="Ioasys logo" />
                    <h4>BEM-VINDO AO EMPRESAS</h4>
                    <p className="loren">Lorem ipsum dolor sit amet, contetur <br />adipiscing elit. Nunc accumsan.</p>
                    <form method="post" onSubmit={this.submitForm.bind(this)} >
                        <input type="text" id="email" placeholder="E-mail" required />
                        <input type="password" id="password" placeholder="Senha" required />
                        <input type="submit" id="submit-button" value="ENTRAR" />
                    </form>
                </div>
            </div>
        );
    }
}

export default withRouter(Login);