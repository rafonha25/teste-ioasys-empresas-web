import React, { Component } from 'react';
import './Search2.css';

class Search2 extends Component {

    constructor() {
        super();
    }

    render() {

        return (
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <input type="search" id="searchbox" placeholder="Pesquisar" />
                </nav>
            </div>
        );
    }
}

export default Search2;
